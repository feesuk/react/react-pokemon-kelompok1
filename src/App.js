import React, { Component } from "react";
import NavBar from "./component/NavBar";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import About from "./screens/About";
import Home from "./screens/Home";
import Pokedex from "./screens/Pokedex";

export default class App extends Component {
	render() {
		return (
			<Router>
				<div className="header">
					<NavBar />
				</div>
				<Switch>
					<Route component={Home} path="/" exact={true} />
					<Route component={About} path="/about" />
					<Route component={Pokedex} path="/pokedex" />
				</Switch>
			</Router>
		);
	}
}
