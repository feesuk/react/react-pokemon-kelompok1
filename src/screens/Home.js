import React, { Component } from "react";
import axios from "axios";
import "./Home.css";

export default class Home extends Component {
	constructor() {
		super();
		this.state = {
			data: [],
		};
	}

	componentDidMount() {
		axios.get("https://pokeapi.co/api/v2/pokemon/").then(response => {
			this.setState({ data: response.data.results });
		});
	}

	render() {
		const { data } = this.state;

		return (
			<div className="container">
				{data.map((item, index) => (
					<div key={index} className="pokemon-item">
						<img
							src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${
								index + 1
							}.png`}
							alt={item.name}
						/>
						<h2>{item.name}</h2>
					</div>
				))}
			</div>
		);
	}
}
