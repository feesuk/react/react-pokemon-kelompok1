import React from "react";
import "./NavBar.css";
import logo from "../images/pokemon.jpg";

export default function NavBar() {
	return (
		<div className="navigationbar">
			<div className="logo">
				<a href="/">
					<img
						className="home-logo"
						src={logo}
						alt="logo"
					/>
				</a>
			</div>
			<div className="navigation">
				<ul>
					<li>
						<a href="/pokedex">Pokedex</a>
					</li>
					<li>
						<a href="/about">About</a>
					</li>
				</ul>
			</div>
		</div>
	);
}
